@extends('layouts.frontend')

@section('title', 'Jasa')

@section('content')
    <div class="container">
        <section>
            <h1>Keluhan {{ $keluhan->keluhan }}</h1>
            <p>{{ $keluhan->deskripsi }}</p>
            <hr>
        </section>
        <section class="">
            <h2 class="mb-1">Semua Jasa Terkait</h2>
            <div class="row match-height">
                @foreach($detail as $value)
                    <div class="col-xl-3 col-md-6 col-sm-12">
                        <a href="{{ route('detail-jasa', $value->id) }}">
                            <div class="card">
                                <div class="card-content">
                                    <img class="card-img-top img-fluid" src="{{ asset('storage/' . $value->image ) }}" alt="Card image cap">
                                    <div class="card-body">
                                        <h5>{{ $value->nama }}</h5>
                                        <p class="card-text light">Oleh {{ $value->bengkel->nama }}</p>
                                    </div>
                                    <div class="card-footer bg-white">
                                        <p class="font-medium-2">{{ \App\Helpers\Helper::rupiah($value->harga) }}</p>
                                        <p class="mb-0"><i class="feather icon-map-pin"></i> {{ $value->bengkel->kota->nama }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
@endsection
