
@extends('layouts/admin')

@section('title', 'Suku Cadang')
@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">


    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
@endsection
@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row match-height d-flex justify-content-center">
            <div class="col-md-6 col-12">
                @if(session('status'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{session('status')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @if ($edit)
                        <form enctype="multipart/form-data" class="form form-horizontal" action="{{route('suku-cadang.update', $data->id)}}" method="post">
                            @method('PUT')
                @else
                        <form enctype="multipart/form-data" class="form form-horizontal" action="{{route('suku-cadang.store')}}" method="post">
                @endif
                    @csrf
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $edit ? 'Edit Suku Cadang' : 'Tambah Suku Cadang' }}</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Image</span>
                                                </div>
                                                <div class="col-md-8">
                                                    @if ($edit)
                                                        <img src="{{asset('storage/' . $data->image)}}" class="rounded-circle img-border box-shadow-1 mb-2" width="85px" height="85px">
                                                    @endif

                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="image" id="image"  required>
                                                        <label class="custom-file-label" for="image"></label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Nama Suku Cadang</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" id="first-name" name="name" class="form-control" value="{{ $edit ? $data->nama : old('name') }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Deskripsi</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <textarea id="deskripsi" name="deskripsi" class="form-control" required>{{ $edit ? $data->deskripsi : old('deskripsi') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Harga</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="number" id="harga" name="harga" class="form-control" value="{{ $edit ? $data->harga : old('harga') }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Pemilik Bengkel</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <select name="bengkel_id" class="form-control" id="basicSelect">
                                                        @foreach($bengkel as $value)
                                                            <option value="{{ $value->id }}" {{ ($edit && $value->id == $data->bengkel_id) ? 'selected' : '' }}>{{ $value->nama }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Pilih merk</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <select name="merk_id" class="form-control" id="merk">
                                                        @foreach($merk as $value)                                                <option value="{{ $value->id }}" {{ ($edit && $value->id == $data->merk_id) ? 'selected' : '' }}>{{ $value->nama }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Pilih tipe</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <select name="tipe_id" class="form-control" id="tipe" required>
                                                        @if ($edit)
                                                            @foreach($tipe as $value)
                                                                <option value="{{ $value->id }}" {{ ($edit && $value->id == $data->merk_id) ? 'selected' : '' }}>{{ $value->nama }}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="">-- pilih tipe --</option>
                                                        @endif

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Seri</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <select name="seri_id" class="form-control" id="seri" required>
                                                        @if ($edit)
                                                            @foreach($seri as $value)
                                                                <option value="{{ $value->id }}" {{ ($edit && $value->id == $data->seri_id) ? 'selected' : '' }}>{{ $value->nama }}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="">-- pilih seri --</option>
                                                        @endif

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted">
                        <span class="float-left">
                            <button type="reset" class="btn btn-outline-warning">Reset</button>
                        </span>
                        <span class="float-right">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </span>
                    </div>
                </div>
                </form>
            </div>
    </section>

@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/editors/quill/katex.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/highlight.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/quill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>

    <script>
        $("#merk").on('change', function () {
            var idMerkSelected = $("#merk").val();
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/ajax/get-tipe-by-merk/" + idMerkSelected,
                success: function (data) {
                    console.log(data)
                    $("#tipe").html(data);
                }
            })
        })

        $("#tipe").on('change', function () {
            var idTipeSelected = $("#tipe").val();
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/ajax/get-seri-by-tipe/" + idTipeSelected,
                success: function (data) {
                    console.log(data)
                    $("#seri").html(data);
                }
            })
        })
    </script>
@endsection

