
@extends('layouts/admin')

@section('title', 'Jasa')
@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">


    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
@endsection
@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row match-height d-flex justify-content-center">
            <div class="col-md-6 col-12">
                @if(session('status'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{session('status')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @if ($edit)
                        <form enctype="multipart/form-data" class="form form-horizontal" action="{{route('jasa.update', $data->id)}}" method="post">
                            @method('PUT')
                @else
                        <form enctype="multipart/form-data" class="form form-horizontal" action="{{route('jasa.store')}}" method="post">
                @endif
                    @csrf
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $edit ? 'Edit jasa' : 'Tambah jasa' }}</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Image</span>
                                                </div>
                                                <div class="col-md-8">
                                                    @if ($edit)
                                                        <img src="{{asset('storage/' . $data->image)}}" class="rounded-circle img-border box-shadow-1 mb-2" width="85px" height="85px">
                                                    @endif

                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="image" id="image" >
                                                        <label class="custom-file-label" for="image"></label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Nama jasa</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" id="first-name" name="name" class="form-control" value="{{ $edit ? $data->nama : old('name') }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Deskripsi</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <textarea id="deskripsi" name="deskripsi" class="form-control" required>{{ $edit ? $data->deskripsi : old('deskripsi') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 d-none">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Harga</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" id="harga" name="harga" class="form-control" value="{{ $edit ? $data->harga : old('harga') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Untuk Keluhan</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <select name="keluhan_id" class="form-control" id="basicSelect">
                                                        <option value="null">Pilih Keluhan</option>
                                                        @foreach($keluhan as $value)
                                                            <option value="{{ $value->id }}" {{ ($edit && $value->id == $data->keluhan_id) ? 'selected' : '' }}>{{ $value->keluhan }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Pemilik Bengkel</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <select name="bengkel_id" class="form-control" id="basicSelect">
                                                        @foreach($bengkel as $value)
                                                            <option value="{{ $value->id }}" {{ ($edit && $value->id == $data->bengkel_id) ? 'selected' : '' }}>{{ $value->nama }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    @if($edit)
                        <div class="card-header">
                            <h4 class="card-title">Harga Jasa</h4> <a href="{{ route('jasa-harga-form', $data->id) }}">+ Tambah</a>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>No</th>
                                        <th>Paket</th>
                                        <th>Deskripsi</th>
                                        <th>Harga</th>
                                        <th></th>
                                    </tr>
                                    @php
                                        $no = 1;
                                    @endphp
                                    @foreach($jasaHarga as $value)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $value->paket }}</td>
                                            <td>{{ $value->deskripsi }}</td>
                                            <td>{{ $value->harga }}</td>
                                            <td>
                                                <a href="{{ route('jasa-harga-edit', ['jasa_id' => $data->id, 'jasa_harga_id' => $value->id]) }}" class="btn btn-primary btn-sm">Edit</a> |
                                                <a href="{{ route('jasa-harga-delete', $value->id) }}" class="btn btn-danger btn-sm">Hapus</a>
                                            </td>
                                        </tr>
                                        @php
                                            $no++;
                                        @endphp
                                    @endforeach
                                </table>
                            </div>
                        </div>

                    @endif
                    <div class="card-footer text-muted">
                        <span class="float-left">
                            <button type="reset" class="btn btn-outline-warning">Reset</button>
                        </span>
                        <span class="float-right">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </span>
                    </div>
                </div>
                </form>
            </div>
    </section>

@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/editors/quill/katex.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/highlight.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/quill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>



    <script>
        var editor = new Quill('#snow-container .editor', {
            bounds: '#snow-container .editor',
            modules: {

                toolbar: [
                    ['bold', 'italic', 'underline'],
                    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        var justHtmlContent = document.getElementById('description');

        editor.on('text-change', function() {
            var justHtml = editor.root.innerHTML;
            justHtmlContent.innerHTML = justHtml;
        });
    </script>
@endsection
