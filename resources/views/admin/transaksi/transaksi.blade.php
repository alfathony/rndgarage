@extends('layouts.admin')

@section('title', 'Transaksi')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header mb-1">
                        <h4 class="card-title">List Transaksi</h4>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <div class="table-responsive">
                                <table class="table table-striped zero-configuration">
                                    <thead>
                                    <tr>
                                        <th>Tgl. Reservasi</th>
                                        <th>Nama Pelanggan</th>
                                        <th>Pesanan</th>
                                        <th>Bengkel</th>
                                        <th>Paket</th>
                                        <th>Total</th>
                                        <th>Pembayaran</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($datas as $data)

                                        <tr>
                                            <td>{{ $data->created_at }}</td>
                                            <td>{{ $data->nama_pelanggan }}</td>
                                            @if ($data->jasa)
                                                <td>Jasa: {{ $data->jasa['nama'] }}</td>
                                            @else
                                                <td>Suku Cadang: {{ $data->sukucadang['nama'] }}</td>
                                            @endif

                                            <td>{{ $data->bengkel->nama }}</td>
                                            <td>{{ $data->nama_paket }}</td>
                                            <td>{{ Helper::rupiah($data->total) }}</td>
                                            <td>{{ $data->status_pembayaran }}</td>
                                            <td>
                                                <a href="{{ route('transaksi.show', $data->id) }}" class="btn btn-primary btn-sm">Lihat Detail</a>

                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>

                                </table>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
@endsection
