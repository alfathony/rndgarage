
@extends('layouts/admin')

@section('title', 'Transaksi')
@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">


    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
@endsection
@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row match-height d-flex justify-content-center">
            <div class="col-md-6 col-12">
                @if(session('status'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{session('status')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif


                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Detail Transaksi</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="form-body">
                                <div class="row mb-2">
                                    <div class="col-md-4"><strong>Nama Pelanggan</strong></div>
                                    <div class="col-md-8">{{ $data->nama_pelanggan }}</div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4"><strong>Tgl. Reservasi</strong></div>
                                    <div class="col-md-8">{{ $data->created_at }}</div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4"><strong>Nama Bengkel</strong></div>
                                    <div class="col-md-8">{{ $data->bengkel->nama }}</div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4"><strong>Nama Pesanan</strong></div>
                                    @if ($data->jasa)
                                        <div class="col-md-8">Jasa: {{ $data->jasa->nama }}</div>
                                    @else
                                        <div class="col-md-8">Suku Cadang: {{ $data->sukucadang->nama }}</div>
                                    @endif
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4"><strong>Email</strong></div>
                                    <div class="col-md-8">{{ $data->email }}</div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4"><strong>Telepon</strong></div>
                                    <div class="col-md-8">{{ $data->telepon }}</div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4"><strong>Alamat</strong></div>
                                    <div class="col-md-8">{{ $data->alamat }}</div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4"><strong>Tanggal Kedatangan</strong></div>
                                    <div class="col-md-8">{{ $data->tanggal_kedatangan }}</div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4"><strong>Paket</strong></div>
                                    <div class="col-md-8">{{ $data->nama_paket }}</div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4"><strong>Total</strong></div>
                                    <div class="col-md-8">{{ \App\Helpers\Helper::rupiah($data->total) }}</div>
                                </div>
                                <div class="row mb-2">
                                    <div class="col-md-4"><strong>Status Pembayaran</strong></div>
                                    <div class="col-md-8">{{ $data->status_pembayaran  }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @cannot('isCustomer')
                    <div class="card-footer text-muted">
                        <span class="float-right">
                            <a href="{{ route('transaksi.edit', $data->id) }}" class="btn btn-primary">Edit</a>
                        </span>
                    </div>
                    @endcannot
                </div>

            </div>
    </section>

@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/editors/quill/katex.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/highlight.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/quill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>



    <script>
        var editor = new Quill('#snow-container .editor', {
            bounds: '#snow-container .editor',
            modules: {

                toolbar: [
                    ['bold', 'italic', 'underline'],
                    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        var justHtmlContent = document.getElementById('description');

        editor.on('text-change', function() {
            var justHtml = editor.root.innerHTML;
            justHtmlContent.innerHTML = justHtml;
        });
    </script>
@endsection
