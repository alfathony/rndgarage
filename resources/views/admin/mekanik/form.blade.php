
@extends('layouts/admin')

@section('title', 'Mekanik')
@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">


    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
@endsection
@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row match-height d-flex justify-content-center">
            <div class="col-md-6 col-12">
                @if(session('status'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{session('status')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @if ($edit)
                        <form enctype="multipart/form-data" class="form form-horizontal" action="{{route('mekanik.update', $data->id)}}" method="post">
                            @method('PUT')
                @else
                        <form enctype="multipart/form-data" class="form form-horizontal" action="{{route('mekanik.store')}}" method="post">
                @endif
                    @csrf
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $edit ? 'Edit Mekanik' : 'Tambah Mekanik' }}</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                                <div class="form-body">
                                    <div class="row">

                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Nama Mekanik</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" id="first-name" name="name" class="form-control" value="{{ $edit ? $data->nama : old('name') }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Bekerja di bengkel</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <select name="bengkel_id" class="form-control" id="basicSelect">
                                                        @foreach($bengkel as $value)
                                                            <option value="{{ $value->id }}" {{ ($edit && $value->id == $data->bengkel_id) ? 'selected' : '' }}>{{ $value->nama }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Telepon</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" id="telepon" name="telepon" class="form-control" value="{{ $edit ? $data->telepon : old('telepon') }}" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Pengalaman</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <textarea id="pengalaman" name="pengalaman" class="form-control" required>{{ $edit ? $data->pengalaman : old('pengalaman') }}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Sejak Tahun</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <select name="sejak_tahun" class="form-control" id="sejak_tahun">
                                                        @for ($i = 0; $i < 40 ; $i++)
                                                            <option value="{{ date('Y') - $i }}" >{{ date('Y') - $i }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted">
                        <span class="float-left">
                            <button type="reset" class="btn btn-outline-warning">Reset</button>
                        </span>
                        <span class="float-right">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </span>
                    </div>
                </div>
                </form>
            </div>
    </section>

@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/editors/quill/katex.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/highlight.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/quill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>

    <script>
        $("#merk").on('change', function () {
            var idMerkSelected = $("#merk").val();
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/ajax/get-tipe-by-merk/" + idMerkSelected,
                success: function (data) {
                    console.log(data)
                    $("#tipe").html(data);
                }
            })
        })

        $("#tipe").on('change', function () {
            var idTipeSelected = $("#tipe").val();
            $.ajax({
                type: "GET",
                url: "{{ URL::to('/') }}/ajax/get-seri-by-tipe/" + idTipeSelected,
                success: function (data) {
                    console.log(data)
                    $("#seri").html(data);
                }
            })
        })
    </script>
@endsection

