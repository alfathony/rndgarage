
@extends('layouts/admin')

@section('title', 'Details Banner')

@section('content')

    <section id="basic-vertical-layouts">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$banner->title}}</h4>
                    <div class="button-header">
                    <button type="button" id="confirm-delete" data-id="{{$banner->id}}" class="btn btn-danger btn-sm">Delete</button>
                    <a href="{{route('banner.edit', $banner->id)}}" class="btn btn-primary btn-sm">Change</a>
                    </div>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <img src="{{ asset('storage/' . $banner->image) }}"  class="img-fluid"  alt="product image">
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

@endsection
@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
    <script src="{{asset('/vendors/js/vendors.min.js')}}"></script>
    <script src="{{asset('/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('/vendors/js/extensions/polyfill.min.js')}}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/datatables/datatable.js')) }}"></script>
    <script>
        $('#confirm-delete').on('click', function () {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                confirmButtonClass: 'btn btn-danger float-right',
                cancelButtonClass: 'btn btn-default ml-1 float-left',
                buttonsStyling: false,
            }).then(function (result) {
                if (result.value) {
                    $.ajax({
                        type: "DELETE",
                        url: "{{ route('banner.destroy', $banner->id) }}",
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        success: function (data) {
                            Swal.fire(
                                {
                                    type: "success",
                                    title: 'Deleted Succeed',
                                    text: 'Your data has been deleted.',
                                    confirmButtonClass: 'btn btn-success',
                                }
                            ).then(function () {
                                window.location.href="{{ route('banner.index') }}";
                            });
                        }
                    }).fail(function(jqXHR, textStatus){
                        Swal.fire(
                            {
                                type: "error",
                                title: 'Failed',
                                text: 'Something went wrong, please try again!',
                                confirmButtonClass: 'btn btn-success',
                            }
                        )
                    })
                }
            })
        });

    </script>
@endsection
