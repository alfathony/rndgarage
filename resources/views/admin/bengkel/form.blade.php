
@extends('layouts/admin')

@section('title', 'Bengkel')
@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">


    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
@endsection
@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row match-height d-flex justify-content-center">
            <div class="col-md-6 col-12">
                @if(session('status'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{session('status')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @if ($edit)
                        <form enctype="multipart/form-data" class="form form-horizontal" action="{{route('bengkel.update', $data->id)}}" method="post">
                            @method('PUT')
                @else
                        <form enctype="multipart/form-data" class="form form-horizontal" action="{{route('bengkel.store')}}" method="post">
                @endif
                    @csrf
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $edit ? 'Edit bengkel' : 'Tambah bengkel' }}</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Logo</span>
                                                </div>
                                                <div class="col-md-8">
                                                    @if ($edit)
                                                        <img src="{{asset('storage/' . $data->logo)}}" class="rounded-circle img-border box-shadow-1 mb-2" width="85px" height="85px">
                                                    @endif

                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="logo" id="avatar">
                                                        <label class="custom-file-label" for="avatar"></label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Nama bengkel</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" id="first-name" name="name" class="form-control" value="{{ $edit ? $data->nama : old('name') }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Deskripsi</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <textarea id="deskripsi" name="deskripsi" class="form-control" required>{{ $edit ? $data->deskripsi : old('deskripsi') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Alamat</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <textarea id="alamat" name="alamat" class="form-control" required>{{ $edit ? $data->alamat : old('alamat') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Kota</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <select name="kota_id" class="form-control" id="basicSelect">
                                                        @foreach($kota as $value)
                                                            <option value="{{ $value->id }}" {{ ($edit && $value->id == $data->master_kota_id) ? 'selected' : '' }}>{{ $value->nama }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Telepon</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" id="telepon" name="telepon" class="form-control" value="{{ $edit ? $data->telepon : old('telepon') }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Alamat Email</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="email" id="email" name="email" class="form-control" value="{{ $edit ? $data->email : old('email') }}" required>
                                                </div>
                                            </div>
                                        </div>

                                        @cannot('isBengkel')
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Pemilik Bengkel</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <select name="user_id" class="form-control" id="basicSelect">
                                                        @foreach($users as $value)
                                                            <option value="{{ $value->id }}" {{ ($edit && $value->id == $data->user_id) ? 'selected' : '' }}>{{ $value->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        @else
                                            <input type="hidden" name="user_id" value="{{ \Illuminate\Support\Facades\Auth::user()->id }}">
                                        @endcannot
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted">
                        <span class="float-left">
                            <button type="reset" class="btn btn-outline-warning">Reset</button>
                        </span>
                        <span class="float-right">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </span>
                    </div>
                </div>
                </form>
            </div>
    </section>

@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/editors/quill/katex.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/highlight.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/quill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>



    <script>
        var editor = new Quill('#snow-container .editor', {
            bounds: '#snow-container .editor',
            modules: {

                toolbar: [
                    ['bold', 'italic', 'underline'],
                    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        var justHtmlContent = document.getElementById('description');

        editor.on('text-change', function() {
            var justHtml = editor.root.innerHTML;
            justHtmlContent.innerHTML = justHtml;
        });
    </script>
@endsection
