@extends('layouts.frontend')

@section('title', 'Jasa')

@section('content')
    <div class="container">
        <section class="">
            <div class="row match-height">
                    <div class="col-md-6 col-sm-12">
                        <div class="card">
                            <div class="card-content">
                                <img class="card-img-top img-fluid" src="{{ asset('storage/' . $jasa->image ) }}" alt="Card image cap">
                            </div>
                        </div>
                    </div>
                <div class="col-md-6 col-sm-12">
                    <h2 class="mb-3">{{ $jasa->nama }}</h2>
                    <p class="">Oleh <a href="{{ route('detail-bengkel', ['bengkel_id' => $jasa->bengkel_id, 'slug' => \Illuminate\Support\Str::slug($jasa->bengkel->nama)]) }}">{{ $jasa->bengkel->nama }}</a></p>
                    <p class="mb-0"><i class="feather icon-map-pin"></i> {{ $jasa->bengkel->kota->nama }}</p>
                    <hr>
                    <h4>Deskripsi</h4>
                    {{ $jasa->deskripsi }}
                    <hr>
                    <h4 class="mb-2">Paket Harga</h4>
                    @if (0 != count($jasa->paket))
                        @foreach($jasa->paket as $value)
                            <div class="card p-2 mb-1">
                                <h5>{{ $value->paket }} <span class="float-right">{{ \App\Helpers\Helper::rupiah($value->harga) }}</span></h5>
                                <p>{{ $value->deskripsi }}</p>
                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-light">Tidak ada paket harga</div>
                    @endif
                    <hr>
                    @guest
                        <a href="/login" class="btn btn-block btn-primary btn-lg">Reservasi Sekarang</a>
                    @else
                        <a href="{{ route('reservasi-jasa', $jasa->id) }}" class="btn btn-block btn-primary btn-lg">Reservasi Sekarang</a>
                    @endguest
                </div>
            </div>
        </section>

        <section class="my-5">
            <h2 class="mb-3">Jasa Lainnya</h2>
            <div class="row match-height">
                @foreach($jasaLainnya as $value)
                    <div class="col-xl-3 col-md-6 col-sm-12">
                        <a href="{{ route('detail-jasa', $value->id) }}">
                            <div class="card">
                                <div class="card-content">
                                    <img class="card-img-top img-fluid" src="{{ asset('storage/' . $value->image ) }}" alt="Card image cap">
                                    <div class="card-body">
                                        <h5>{{ $value->nama }}</h5>
                                        <p class="card-text light">Oleh {{ $value->bengkel->nama }}</p>
                                    </div>
                                    <div class="card-footer bg-white">
                                        <p class="font-medium-2">{{ $value->hargaMulai }}</p>
                                        <p class="mb-0"><i class="feather icon-map-pin"></i> {{ $value->bengkel->kota->nama }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
@endsection
