@extends('layouts.frontend')

@section('title', 'Detail Bengkel')

@section('content')
    <div class="container">
        <section class="">
            <div class="row">
                <div class="col-md-2">
                    <div class="d-flex justify-content-center">
                    <img src="{{ asset('storage/' . $bengkel->logo) }}" alt="{{ $bengkel->nama }}" class="img-thumbnail img-fluid text-center mb-2">
                    </div>
                </div>
                <div class="col-md-10">
                    <h1>{{ $bengkel->nama }}</h1>
                    <p>{{ $bengkel->deskripsi }}</p>
                    <p>{{ $bengkel->alamat }}</p>
                    <p class="mb-0"><i class="feather icon-map-pin"></i> {{ $bengkel->kota->nama }}</p>
                    <p class="mb-0"><i class="feather icon-phone-call"></i> {{ $bengkel->telepon }}</p>
                    <p class="mb-0"><i class="feather icon-at-sign"></i> {{ $bengkel->email }}</p>
                </div>
            </div>
            <hr>
        </section>

        <section class="my-5">
            <h2 class="mb-3">Jasa Reservasi</h2>
            @if (count($jasa) > 0)
            <div class="row match-height">
                @foreach($jasa as $value)
                    <div class="col-xl-3 col-md-6 col-sm-12">
                        <a href="{{ route('detail-jasa', $value->id) }}">
                            <div class="card">
                                <div class="card-content">
                                    <img class="card-img-top img-fluid" src="{{ asset('storage/' . $value->image ) }}" alt="Card image cap">
                                    <div class="card-body">
                                        <h5>{{ $value->nama }}</h5>
                                        <p class="card-text light">Oleh {{ $value->bengkel->nama }}</p>
                                    </div>
                                    <div class="card-footer bg-white">
                                        <p class="font-medium-2">{{ \App\Helpers\Helper::rupiah($value->harga) }}</p>
                                        <p class="mb-0"><i class="feather icon-map-pin"></i> {{ $value->bengkel->kota->nama }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            @else
                <div class="alert alert-light">Tidak ada jasa reservasi</div>
            @endif
        </section>

        <section class="my-5">
            <h2 class="mb-3">Suku Cadang</h2>
            @if (count($sukucadang) > 0)
            <div class="row match-height">
                    @foreach($sukucadang as $value)
                        <div class="col-xl-3 col-md-6 col-sm-12">
                            <a href="{{ route('detail-suku-cadang', $value->id) }}">
                                <div class="card">
                                    <div class="card-content">
                                        <img class="card-img-top img-fluid" src="{{ asset('storage/' . $value->image ) }}" alt="Card image cap">
                                        <div class="card-body">
                                            <h5>{{ $value->nama }}</h5>
                                            <p class="card-text light"><i class="feather icon-at-sign"></i> {{ $value->merk->nama }}</p>
                                            <p class="font-medium-2 mb-0">{{ \App\Helpers\Helper::rupiah($value->harga) }}</p>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <p class="card-text light">Oleh {{ $value->bengkel->nama }}</p>
                                            <p class="mb-0"><i class="feather icon-map-pin"></i> {{ $value->bengkel->kota->nama }}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
            </div>
                @else
                    <div class="alert alert-light">Tidak ada suku cadang</div>
                @endif
        </section>

        <section class="my-5">
            <h2 class="mb-3">Mekanik</h2>
            @if (count($bengkel->mekanik) > 0)
                <div class="row match-height">
                    @foreach($bengkel->mekanik as $value)
                        <div class="col-xl-3 col-md-6 col-sm-12">
                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <h5>{{ $value->nama }}</h5>
                                            <p class="card-text light"><i class="feather icon-star-on warning"></i> Sejak {{ $value->sejak_tahun }}</p>
                                            <p class="card-text light">{{ $value->pengalaman }}</p>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="alert alert-light">Tidak ada mekanik</div>
            @endif
        </section>
    </div>
@endsection
