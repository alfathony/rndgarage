@extends('layouts.frontend')

@section('title', 'Booking Suku Cadang')

@section('content')
    <div class="container">
        <section class="">
            <div class="row match-height">
                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-content">
                                <img class="card-img-top img-fluid img-thumbnail" src="{{ asset('storage/' . $jasa->image ) }}" alt="Card image cap">
                            </div>
                        </div>
                    </div>
                <div class="col-md-8 col-sm-12">
                    <h2 class="">{{ $jasa->nama }}</h2>
                    <p class="">Oleh {{ $jasa->bengkel->nama }}</p>
                    <hr>
                    <h4 class="mb-2">Data Booking</h4>
                    @if(session('status'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('status')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <form action="{{ route('booking-suku-cadang', $jasa->id) }}" method="POST">
                        @csrf
                        <input type="hidden" name="jasa_id" value="{{ $jasa->id }}">
                        <input type="hidden" name="bengkel_id" value="{{ $jasa->bengkel->id }}">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Nama pelanggan</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="" name="name" class="form-control" value="{{ $user->name }}" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Alamat email</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="email" id="" name="email" class="form-control" value="{{ $user->email }}" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Telepon</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="" name="telepon" class="form-control" value="" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Alamat</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="" name="alamat" class="form-control" value="" required="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class=" row">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-8">
                                        <button type="submit" class="btn btn-block btn-primary btn-lg">Kirim</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </section>

    </div>
@endsection
