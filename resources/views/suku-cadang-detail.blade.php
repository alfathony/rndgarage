@extends('layouts.frontend')

@section('title', 'Suku Cadang')

@section('content')
    <div class="container">
        <section class="">
            <div class="row match-height">
                    <div class="col-md-6 col-sm-12">
                        <div class="card">
                            <div class="card-content">
                                <img class="card-img-top img-fluid" src="{{ asset('storage/' . $data->image ) }}" alt="Card image cap">
                            </div>
                        </div>
                    </div>
                <div class="col-md-6 col-sm-12">
                    <h2 class="mb-3">{{ $data->nama }}</h2>
                    <p class="">Oleh <a href="{{ route('detail-bengkel', ['bengkel_id' => $data->bengkel_id, 'slug' => \Illuminate\Support\Str::slug($data->bengkel->nama)]) }}">{{ $data->bengkel->nama }}</a></p>
                    <p class=""><i class="feather icon-at-sign"></i> {{ $data->merk->nama . " " . $data->tipe->nama . " " . $data->seri->nama }}</p>
                    <p class="mb-0"><i class="feather icon-map-pin"></i> {{ $data->bengkel->kota->nama }}</p>
                    <h4 class="mt-2">{{ \App\Helpers\Helper::rupiah($data->harga) }}</h4>
                    <hr>
                    <h4>Deskripsi</h4>
                    {{ $data->deskripsi }}
                    <hr>
                    @guest
                        <a href="/login" class="btn btn-block btn-primary btn-lg">Booking Sekarang</a>
                    @else
                        <a href="{{ route('booking-suku-cadang', $data->id) }}" class="btn btn-block btn-primary btn-lg">Booking Sekarang</a>
                    @endguest
                </div>
            </div>
        </section>

        <section class="my-5">
            <h2 class="mb-3">Suku Cadang Lainnya</h2>
            <div class="row match-height">
                @foreach($dataLainnya as $value)
                    <div class="col-xl-3 col-md-6 col-sm-12">
                        <a href="{{ route('detail-suku-cadang', $value->id) }}">
                            <div class="card">
                                <div class="card-content">
                                    <img class="card-img-top img-fluid" src="{{ asset('storage/' . $value->image ) }}" alt="Card image cap">
                                    <div class="card-body">
                                        <h5>{{ $value->nama }}</h5>
                                        <p class="card-text light"><i class="feather icon-at-sign"></i> {{ $value->merk->nama }}</p>
                                        <p class="font-medium-2 mb-0">{{ \App\Helpers\Helper::rupiah($value->harga) }}</p>
                                    </div>
                                    <div class="card-footer bg-white">
                                        <p class="card-text light">Oleh {{ $value->bengkel->nama }}</p>
                                        <p class="mb-0"><i class="feather icon-map-pin"></i> {{ $value->bengkel->kota->nama }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
@endsection
