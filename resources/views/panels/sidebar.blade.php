@php
$configData = Helper::applClasses();
@endphp
<div
  class="main-menu menu-fixed {{($configData['theme'] === 'light') ? "menu-light" : "menu-dark"}} menu-accordion menu-shadow"
  data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto">
        <a class="navbar-brand" href="{{ route('dashboard') }}">
          <div class="brand-logo"></div>
          <h2 class="brand-text mb-0">Garasi Klasik</h2>
        </a>
      </li>
      <li class="nav-item nav-toggle">
        <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
          <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
          <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block primary collapse-toggle-icon"
            data-ticon="icon-disc">
          </i>
        </a>
      </li>
    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

        <li class="nav-item {{ (request()->routeIs('dashboard')  ) ? 'active' : '' }}">
            <a href="{{ route('dashboard') }}">
                <i class="feather icon-home"></i>
                <span class="menu-title" data-i18n="">Dashboard</span>
            </a>
        </li>

        @cannot('isCustomer')
        <li class="nav-item {{ (request()->routeIs('bengkel.*')  ) ? 'active' : '' }}">
            <a href="{{ route('bengkel.index') }}">
                <i class="feather icon-settings"></i>
                <span class="menu-title" data-i18n="">Bengkel</span>
            </a>
        </li>

            <li class="nav-item not-active">
                <a href="{{ route('mekanik.index') }}">
                    <i class="feather icon-activity"></i>
                    <span class="menu-title" data-i18n="">Mekanik</span>
                </a>
            </li>

        <li class="nav-item not-active">
            <a href="{{ route('jasa.index') }}">
                <i class="feather icon-shopping-bag"></i>
                <span class="menu-title" data-i18n="">Jasa</span>
            </a>
        </li>

        <li class="nav-item not-active">
            <a href="{{ route('suku-cadang.index') }}">
                <i class="feather icon-box"></i>
                <span class="menu-title" data-i18n="">Suku Cadang</span>
            </a>
        </li>

        @endcannot

        <li class="nav-item not-active">
            <a href="{{ route('transaksi.index') }}">
                <i class="feather icon-shopping-cart"></i>
                <span class="menu-title" data-i18n="">Transaksi</span>
            </a>
        </li>

        @can('isAdmin')
        <li class="navigation-header">
            <span>Master Data</span>
        </li>

            <li class="nav-item {{ (request()->routeIs('banner.*')  ) ? 'active' : '' }}">
                <a href="{{ route('banner.index') }}">
                    <i class="feather icon-database"></i>
                    <span class="menu-title" data-i18n="">Banner</span>
                </a>
            </li>

        <li class="nav-item not-active">
            <a href="#">
                <i class="feather icon-database"></i>
                <span class="menu-title" data-i18n="">Mobil</span>
            </a>

            <ul class="menu-content">
                <li class="{{ (request()->routeIs('merk.*')  ) ? 'active' : '' }}">
                    <a href="{{ route('merk.index') }}">
                        <span class="menu-title" data-i18n="">Merk</span>
                    </a>
                </li>
                <li class="{{ (request()->routeIs('tipe.*')  ) ? 'active' : '' }}">
                    <a href="{{ route('tipe.index') }}">
                        <span class="menu-title" data-i18n="">Tipe</span>
                    </a>
                </li>
                <li class="{{ (request()->routeIs('seri.*')  ) ? 'active' : '' }}">
                    <a href="{{ route('seri.index') }}">
                        <span class="menu-title" data-i18n="">Seri</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item {{ (request()->routeIs('kota.*')  ) ? 'active' : '' }}">
            <a href="{{ route('kota.index') }}">
                <i class="feather icon-database"></i>
                <span class="menu-title" data-i18n="">Kota</span>
            </a>
        </li>

            <li class="nav-item {{ (request()->routeIs('keluhan.*')  ) ? 'active' : '' }}">
                <a href="{{ route('keluhan.index') }}">
                    <i class="feather icon-database"></i>
                    <span class="menu-title" data-i18n="">Keluhan</span>
                </a>
            </li>

        <li class="nav-item not-active">
            <a href="{{ route('users.index') }}">
                <i class="feather icon-users"></i>
                <span class="menu-title" data-i18n="">Users</span>
{{--                    <span class="badge badge-pill badge-primary float-right notTest">9+</span>--}}
            </a>
        </li>

        <li class="nav-item not-active">
            <a href="{{ route('roles.index') }}">
                <i class="feather icon-shield"></i>
                <span class="menu-title" data-i18n="">Roles</span>
                {{--                    <span class="badge badge-pill badge-primary float-right notTest">9+</span>--}}
            </a>
        </li>

        @endcan

{{--        <li class="nav-item not-active">--}}
{{--            <a href="#">--}}
{{--                <i class="feather icon-external-link"></i>--}}
{{--                <span class="menu-title" data-i18n="">Dropdown</span>--}}
{{--            </a>--}}

{{--            <ul class="menu-content">--}}
{{--                <li class="not-active">--}}
{{--                    <a href="#">--}}
{{--                        <i class="feather icon-user"></i>--}}
{{--                        <span class="menu-title" data-i18n="">Submenu</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </li>--}}

    </ul>
  </div>
</div>
<!-- END: Main Menu-->
