@if($configData["mainLayoutType"] == 'horizontal' && isset($configData["mainLayoutType"]))
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu {{ $configData['navbarColor'] }} navbar-fixed">
  <div class="navbar-header d-xl-block d-none">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item"><a class="navbar-brand" href="{{ route('home') }}">
          <div class="brand-logo"></div>
              <h2 class="brand-text mb-0">Garasi Klasik</h2>
          </a>
      </li>
    </ul>
  </div>
  @else
  <nav
    class="header-navbar navbar-expand-lg navbar navbar-with-menu {{ $configData['navbarClass'] }} navbar-light navbar-shadow {{ $configData['navbarColor'] }}">
    @endif
    <div class="navbar-wrapper">
      <div class="navbar-container content">
        <div class="navbar-collapse" id="navbar-mobile">
          <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
            <ul class="nav navbar-nav">
              <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs"
                  href="#"><i class="ficon feather icon-menu"></i></a></li>
            </ul>
          </div>
          <ul class="nav navbar-nav float-right">



              @guest
                  <li class="dropdown dropdown-user ">
                      <div class="p-1">
                          <a href="{{ route('login') }}" class="btn bg-gradient-primary waves-effect waves-light"><i class="feather icon-user"></i>Sign In</a>
                      </div>
                  </li>
              @else
                  <li class="dropdown dropdown-user nav-item">
                      <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                          <div class="user-nav d-sm-flex d-none">
                              <span class="user-name text-bold-600">{{ Auth::user()->name }}</span>
                              <span class="user-status">{{ Auth::user()->role->name }}</span>
                          </div>
                          <span>
                              @if (Auth::user()->avatar)
                                  <img class="round" src="{{ asset('storage/' . Auth::user()->avatar) }}" alt="avatar" height="40" width="40" />
                              @endif
                          </span>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right">
                          <a class="dropdown-item" href="{{ route('dashboard') }}" target="_blank"><i class="feather icon-home"></i> Dashboard</a>
                              <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                  class="feather icon-power"></i> Logout</a>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                      </div>
                  </li>
              @endguest
          </ul>
        </div>
      </div>
    </div>
  </nav>
