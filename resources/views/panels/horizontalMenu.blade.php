@php
    $configData = Helper::applClasses();
@endphp
{{-- Horizontal Menu --}}
<div class="horizontal-menu-wrapper">
    <div class="header-navbar navbar-expand-sm navbar navbar-horizontal {{$configData['horizontalMenuClass']}} {{($configData['theme'] === 'light') ? "navbar-light" : "navbar-dark" }} navbar-without-dd-arrow navbar-shadow navbar-brand-center"
         role="navigation" data-menu="menu-wrapper" data-nav="brand-center">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="dashboard-analytics">
                        <div class="brand-logo"></div>
                        <h2 class="brand-text mb-0">Garasi Klasik</h2>
                    </a></li>
                <li class="nav-item nav-toggle">
                    <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                        <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                        <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary"
                           data-ticon="icon-disc"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Horizontal menu content-->
        <div class="navbar-container main-menu-content" data-menu="menu-container">
            <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                {{-- Foreach menu item starts --}}
                <li class="nav-item {{ (request()->routeIs('home')  ) ? 'active' : '' }}"
                    >
                    <a href="{{ route('home') }}" class="nav-link">
                        <span >Home</span>
                    </a>
                </li>
                <li class="nav-item {{ (request()->routeIs('list-bengkel') || request()->routeIs('detail-bengkel')  ) ? 'active' : '' }}"
                   >
                    <a href="{{ route('list-bengkel') }}" class="nav-link">
                        <span >Bengkel</span>
                    </a>
                </li>
                <li class="nav-item {{ (request()->routeIs('list-jasa')  ) ? 'active' : '' }}"
                   >
                    <a href="{{ route('list-jasa') }}" class="nav-link">
                        <span >Jasa Restorasi</span>
                    </a>
                </li>
                <li class="nav-item {{ (request()->routeIs('list-suku-cadang')  ) ? 'active' : '' }}"
                    >
                    <a href="{{ route('list-suku-cadang') }}" class="nav-link">
                        <span >Suku Cadang</span>
                    </a>
                </li>
                <li class="nav-item {{ (request()->routeIs('list-keluhan')  ) ? 'active' : '' }}"
                >
                    <a href="{{ route('list-keluhan') }}" class="nav-link">
                        <span >Daftar Keluhan</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
