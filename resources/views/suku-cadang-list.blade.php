@extends('layouts.frontend')

@section('title', 'Suku Cadang')

@section('content')
    <div class="container">
        <section class="">
            <h2 class="mb-1">Semua Suku Cadang</h2>
            <div class="mb-3">
                <form method="get" class="form">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" name="search" class="form-control form-control-lg" placeholder="Cari Suku Cadang" value="{{ isset($_GET['search']) ? $_GET['search'] : '' }}">
                        </div>
                        <div class="col-md-3">
                            <select class="form-control form-control-lg" name="kota">
                                <option value="0">Pilih Kota</option>
                                @foreach($kota as $value)
                                    <option value="{{ $value->id }}" {{ isset($_GET['kota']) && ($_GET['kota'] == $value->id)  ? 'selected': '' }}>{{ $value->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Cari</button>
                        </div>
                    </div>
                </form>
                <hr>
            </div>
            <div class="row match-height">
                @foreach($datas as $value)
                    <div class="col-xl-3 col-md-6 col-sm-12">
                        <a href="{{ route('detail-suku-cadang', $value->id) }}">
                            <div class="card">
                                <div class="card-content">
                                    <img class="card-img-top img-fluid" src="{{ asset('storage/' . $value->image ) }}" alt="Card image cap">
                                    <div class="card-body">
                                        <h5>{{ $value->nama }}</h5>
                                        <p class="card-text light"><i class="feather icon-at-sign"></i> {{ $value->merk->nama }}</p>
                                        <p class="font-medium-2 mb-0">{{ \App\Helpers\Helper::rupiah($value->harga) }}</p>
                                    </div>
                                    <div class="card-footer bg-white">
                                        <p class="card-text light">Oleh {{ $value->bengkel->nama }}</p>
                                        <p class="mb-0"><i class="feather icon-map-pin"></i> {{ $value->bengkel->kota->nama }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
@endsection
