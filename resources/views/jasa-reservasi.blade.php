@extends('layouts.frontend')

@section('title', 'Reservasi Jasa')

@section('content')
    <div class="container">
        <section class="">
            <div class="row match-height">
                    <div class="col-md-4 col-sm-12">
                        <div class="card">
                            <div class="card-content">
                                <img class="card-img-top img-fluid img-thumbnail" src="{{ asset('storage/' . $jasa->image ) }}" alt="Card image cap">
                            </div>
                        </div>
                    </div>
                <div class="col-md-8 col-sm-12">
                    <h2 class="">{{ $jasa->nama }}</h2>
                    <p class="">Oleh {{ $jasa->bengkel->nama }}</p>
                    <hr>
                    <h4 class="mb-2">Data Reservasi</h4>
                    @if(session('status'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{session('status')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <form action="{{ route('reservasi-jasa', $jasa->id) }}" method="POST">
                        @csrf
                        <input type="hidden" name="jasa_id" value="{{ $jasa->id }}">
                        <input type="hidden" name="bengkel_id" value="{{ $jasa->bengkel->id }}">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Nama pelanggan</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="" name="name" class="form-control" value="{{ $user->name }}" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Alamat email</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="email" id="" name="email" class="form-control" value="{{ $user->email }}" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Telepon</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="" name="telepon" class="form-control" value="" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Alamat</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="" name="alamat" class="form-control" value="" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Tanggal kedatangan</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="date" id="" name="tanggal_kedatangan" class="form-control" value="" required="">
                                    </div>
                                </div>
                            </div>


                                <div class="col-12">
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <span>Pilih Paket</span>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="jasa_harga_id" class="form-control">
                                                @if (0 != count($jasa->paket))
                                                    @foreach($jasa->paket as $value)
                                                        <option value="{{ $value->id }}">{{ $value->paket }} {{ \App\Helpers\Helper::rupiah($value->harga) }}</option>
                                                    @endforeach
                                                @else
                                                    <option value="">Tidak ada paket</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>


                            <div class="col-12">
                                <div class=" row">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-8">
                                        <button type="submit" class="btn btn-block btn-primary btn-lg">Kirim</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </section>

    </div>
@endsection
