
@extends('layouts/admin')

@section('title', 'User Detail')
@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">


    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
@endsection
@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row match-height d-flex justify-content-center">
            <div class="col-md-6 col-12">
                @if(session('status'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{session('status')}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form enctype="multipart/form-data" class="form form-horizontal" action="{{route('users.update', $users->id)}}" method="post">
                    @method('PUT')
                    @csrf
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">User Detail</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Avatar</span>
                                                </div>
                                                <div class="col-md-8">
                                                    @if ($users->avatar)
                                                        <img src="{{asset('storage/' . $users->avatar)}}" class="rounded-circle img-border box-shadow-1 mb-2" width="85px" height="85px">
                                                    @endif

                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" name="avatar" id="avatar" >
                                                        <label class="custom-file-label" for="avatar"></label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Name</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="text" id="first-name" name="fullname" class="form-control" value="{{$users->name}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Email</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="email" id="email-id" class="form-control" name="email" value="{{$users->email}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Role User</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <select name="role_id" class="form-control" id="basicSelect">
                                                        @foreach($roles as $role)                                                <option value="{{ $role->id }}" {{ $role->id == $users->role_id ? 'selected' : '' }}>{{ $role->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="card-footer text-muted">
                        <span class="float-left">
                            <button type="reset" class="btn btn-outline-warning">Reset</button>
                        </span>
                        <span class="float-right">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </span>
                    </div>
                </div>
                </form>
            </div>
    </section>

@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>

    <script src="{{ asset(mix('vendors/js/editors/quill/katex.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/highlight.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/quill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/select/form-select2.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>



    <script>
        var editor = new Quill('#snow-container .editor', {
            bounds: '#snow-container .editor',
            modules: {

                toolbar: [
                    ['bold', 'italic', 'underline'],
                    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                    ['clean']
                ]
            },
            theme: 'snow'
        });

        var justHtmlContent = document.getElementById('description');

        editor.on('text-change', function() {
            var justHtml = editor.root.innerHTML;
            justHtmlContent.innerHTML = justHtml;
        });
    </script>
@endsection
