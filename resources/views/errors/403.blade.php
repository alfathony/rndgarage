@extends('layouts/full')

@section('title', 'Not Authorized')

@section('content')
    <!-- maintenance -->
    <section class="row flexbox-container">
        <div class="col-xl-7 col-md-8 col-12 d-flex justify-content-center mx-auto">
            <div class="card auth-card bg-transparent shadow-none rounded-0 mb-0 w-100">
                <div class="card-content">
                    <div class="card-body text-center">
                        <img src="{{ asset('images/pages/not-authorized.png') }}" class="img-fluid align-self-center" alt="branding logo">
                        <h1 class="font-large-2 my-2">You are not authorized!</h1>
                        <a class="btn btn-primary btn-lg mt-2" href="{{ route('home') }}">Back to Home</a> <br>
                        <a class="btn btn-link btn-lg mt-2 text-danger" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">or Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- maintenance end -->
@endsection
