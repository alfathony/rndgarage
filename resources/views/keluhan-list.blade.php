@extends('layouts.frontend')

@section('title', 'Keluhan')

@section('content')
    <div class="container">
        <section class="">
            <h2 class="mb-1">Semua Keluhan</h2>
            <div class="mb-3">
                <form method="get" class="form">
                    <div class="row">
                        <div class="col-md-9">
                            <input type="text" name="search" class="form-control form-control-lg" placeholder="Cari Keluhan" value="{{ isset($_GET['search']) ? $_GET['search'] : '' }}">
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Cari</button>
                        </div>
                    </div>
                </form>
                <hr>
            </div>
            <div class="row match-height">
                @foreach($datas as $value)
                    <div class="col-xl-4 col-md-4 col-sm-6">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h5>{{ $value->keluhan }}</h5>
                                </div>
                                <a href="{{ route('detail-keluhan', [$value->id, \Illuminate\Support\Str::slug($value->keluhan)]) }}">
                                    <div class="card-footer bg-white d-flex justify-content-between">
                                        <span class="card-text light">{{ $value->jasa->count() }} jasa tersedia</span>
                                        <i class="feather icon-chevron-right"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
@endsection
