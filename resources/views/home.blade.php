@extends('layouts.frontend')

@section('title', 'Home')

@section('content')
    <div class="container">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                @php
                $first = true;
                @endphp
                @foreach($banners as $key=>$val)
                    <div class="carousel-item {{ $first ? 'active' : '' }}">
                        <img class="img-fluid" src="{{ asset('storage/' . $val->image) }}" alt="{{ $val->title }}">
                    </div>
                    @if ($first)
                        @php
                            $first = false;
                        @endphp
                    @endif
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <section class="my-5">
            <h2 class="mb-3">Restorasi Rekomendasi <a href="{{ route('list-jasa') }}" class="btn btn-outline-primary float-right">Lihat Semua</a></h2>
            <div class="row match-height">
                @foreach($jasa as $value)
                    <div class="col-xl-3 col-md-6 col-sm-12">
                        <a href="{{ route('detail-jasa', $value->id) }}">
                            <div class="card">
                                <div class="card-content">
                                    <img class="card-img-top img-fluid" src="{{ asset('storage/' . $value->image ) }}" alt="Card image cap">
                                    <div class="card-body">
                                        <h5>{{ $value->nama }}</h5>
                                        <p class="card-text light">Oleh {{ $value->bengkel->nama }}</p>
                                    </div>
                                    <div class="card-footer bg-white">
                                        <p class="font-medium-2">{{ $value->hargaMulai }}</p>
                                        <p class="mb-0"><i class="feather icon-map-pin"></i> {{ $value->bengkel->kota->nama }}</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </section>

        <section class="my-5">
            <h2 class="mb-3">Sparepart Terbaru <a href="{{ route('list-suku-cadang') }}" class="btn btn-outline-primary float-right">Lihat Semua</a></h2>
            <div class="row match-height">
                @foreach($sukucadang as $value)
                <div class="col-xl-3 col-md-6 col-sm-12">
                    <a href="{{ route('detail-suku-cadang', $value->id) }}">
                        <div class="card">
                            <div class="card-content">
                                <img class="card-img-top img-fluid" src="{{ asset('storage/' . $value->image ) }}" alt="Card image cap">
                                <div class="card-body">
                                    <h5>{{ $value->nama }}</h5>
                                    <p class="card-text light"><i class="feather icon-at-sign"></i> {{ $value->merk->nama }}</p>
                                    <p class="font-medium-2 mb-0">{{ \App\Helpers\Helper::rupiah($value->harga) }}</p>
                                </div>
                                <div class="card-footer bg-white">
                                    <p class="card-text light">Oleh {{ $value->bengkel->nama }}</p>
                                    <p class="mb-0"><i class="feather icon-map-pin"></i> {{ $value->bengkel->kota->nama }}</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </section>

        <section class="my-5">
            <h2 class="mb-2">Tersedia di Kota</h2>
            <div class="row">
                @foreach($kota as $value)
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="card-content">
                            <img class="card-img img-fluid" src="{{ asset('/') }}/images/slider/04.jpg" alt="Card image">
                            <div class="card-img-overlay overflow-hidden overlay-danger overlay-lighten-2">
                                <h4 class="card-title text-white">{{ $value->nama }}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </section>

        <section class="my-5">
            <h2 class="mb-2">Apapun mobil klasik mu</h2>
            <div class="row">
                @foreach($merk as $value)
                    <div class="col-xl-2 col-md-4 col-sm-6">
                        <div class="card text-center">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                        <div class="avatar-content">
                                            <img class="round" src="{{ asset('storage/' . $value->logo) }}" alt="avatar" height="52" width="52">
                                        </div>
                                    </div>
                                    <p class="mb-0 line-ellipsis">{{ $value->nama }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>

        <section class="my-5">
            <h2 class="mb-2">Solusi untuk masalah mobilmu</h2>
            <div class="row">
                @foreach($keluhan as $value)
                    <div class="col-xl-4 col-md-4 col-sm-6">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <h5>{{ $value->keluhan }}</h5>
                                </div>
                                <a href="{{ route('detail-keluhan', [$value->id, \Illuminate\Support\Str::slug($value->keluhan)]) }}">
                                <div class="card-footer bg-white d-flex justify-content-between">
                                    <span class="card-text light">{{ $value->jasa->count() }} jasa tersedia</span>
                                    <i class="feather icon-chevron-right"></i>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </div>
@endsection
