/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const appLegacy = new Vue({
    el: '#app',
});

// scroll navbar
var menu = $('.navbar-scrollable');
var origOffsetY = menu.offset().top + 2;

function scroll() {
    if ($(window).scrollTop() >= origOffsetY) {
        $('.navbar').addClass('fixed-top');
        $('.logo-default').show();
        $('.logo-light').hide();
    } else {
        $('.navbar').removeClass('fixed-top');
        $('.logo-default').hide();
        $('.logo-light').show();
    }
}
document.onscroll = scroll;

// carousel owl

$(".carousel").owlCarousel({
    loop: true,
    margin: 16,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 4
        }
    }
});


