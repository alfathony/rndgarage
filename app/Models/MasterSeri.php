<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterSeri extends Model
{
    protected $table = "master_seri";

    public function tipe()
    {
        return $this->belongsTo(MasterTipe::class);
    }
}
