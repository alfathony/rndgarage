<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterMerk extends Model
{
    protected $table = "master_merk";
}
