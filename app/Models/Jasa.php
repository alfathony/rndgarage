<?php

namespace App\Models;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;

class Jasa extends Model
{
    protected $table = 'jasa';

    public function bengkel()
    {
        return $this->belongsTo(Bengkel::class);
    }

    public function paket()
    {
        return $this->hasMany(JasaHarga::class)->orderBy('harga', 'ASC');
    }

    public function keluhan()
    {
        return $this->belongsTo(MasterKeluhan::class, 'keluhan_id', 'id');
    }

    public function getHargaMulaiAttribute()
    {
        if (0 != count($this->paket)){
            return "Mulai ". Helper::rupiah($this->paket[0]->harga);
        }
        else{
            return "Tidak ada harga";
        }
    }
}
