<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Bengkel extends Model
{
    protected $table = "bengkel";

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function kota()
    {
        return $this->belongsTo(MasterKota::class, 'master_kota_id', 'id');
    }

    public function mekanik()
    {
        return $this->hasMany(Mekanik::class);
    }
}
