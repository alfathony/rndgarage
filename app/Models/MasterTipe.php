<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterTipe extends Model
{
    protected $table = "master_tipe";

    public function merk()
    {
        return $this->belongsTo(MasterMerk::class);
    }
}
