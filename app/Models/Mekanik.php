<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mekanik extends Model
{
    protected $table = "mekanik";

    public function bengkel()
    {
        return $this->belongsTo(Bengkel::class);
    }
}
