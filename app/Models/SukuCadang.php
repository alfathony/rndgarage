<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SukuCadang extends Model
{
    protected $table = 'suku_cadang';

    public function bengkel()
    {
        return $this->belongsTo(Bengkel::class);
    }

    public function merk()
    {
        return $this->belongsTo(MasterMerk::class);
    }

    public function tipe()
    {
        return $this->belongsTo(MasterTipe::class);
    }

    public function seri()
    {
        return $this->belongsTo(MasterSeri::class);
    }
}
