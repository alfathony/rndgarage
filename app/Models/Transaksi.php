<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksi';

    public function bengkel()
    {
        return $this->belongsTo(Bengkel::class);
    }

    public function jasa()
    {
        return $this->belongsTo(Jasa::class);
    }

    public function paket()
    {
        return $this->belongsTo(JasaHarga::class);
    }

    public function sukucadang()
    {
        return $this->belongsTo(SukuCadang::class, 'suku_cadang_id', 'id');
    }

    public function getStatusPembayaranAttribute()
    {
        return $this->is_paid == 0 ? 'Belum Lunas' : 'Lunas';
    }
}
