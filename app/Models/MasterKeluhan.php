<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterKeluhan extends Model
{
    protected $table = 'master_keluhan';

    public function jasa()
    {
        return $this->hasMany(Jasa::class, 'keluhan_id', 'id');
    }

}
