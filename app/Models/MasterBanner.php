<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterBanner extends Model
{
    protected $table = "master_banners";
    protected $fillable = ["id","title","image"];

}
