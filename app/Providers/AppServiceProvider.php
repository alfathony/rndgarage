<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(['app.locale' => 'id']);
        Carbon::setLocale('id');

        Gate::define('isCustomer', function($user) {
            return $user->role_id == 1;
        });

        Gate::define('isAdmin', function($user) {
            return $user->role_id == 2;
        });

        Gate::define('isBengkel', function($user) {
            return $user->role_id == 3;
        });
    }
}
