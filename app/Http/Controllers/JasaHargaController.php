<?php

namespace App\Http\Controllers;

use App\Models\Bengkel;
use App\Models\JasaHarga;
use App\Models\MasterKeluhan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JasaHargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($jasa_id)
    {
        $edit = false;

        return view('admin.jasa-harga.form', compact('edit', 'jasa_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new JasaHarga();
        $data->paket = $request->name;
        $data->harga = $request->harga;
        $data->deskripsi = $request->deskripsi;
        $data->jasa_id = $request->jasa_id;

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('jasa.edit', $request->jasa_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($jasa_id, $jasa_harga_id)
    {
        $edit = true;
        $data = JasaHarga::findOrFail($jasa_harga_id);

        return view('admin.jasa-harga.form', compact('edit', 'data', 'jasa_id', 'jasa_harga_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $jasa_id, $jasa_harga_id)
    {
        $data = JasaHarga::findOrFail($jasa_harga_id);
        $data->paket = $request->name;
        $data->harga = $request->harga;
        $data->deskripsi = $request->deskripsi;
        $data->jasa_id = $request->jasa_id;

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('jasa.edit', $request->jasa_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($jasa_harga_id)
    {
        try{
             JasaHarga::destroy($jasa_harga_id);
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->back();
    }
}
