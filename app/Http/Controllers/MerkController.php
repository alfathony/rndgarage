<?php

namespace App\Http\Controllers;

use App\Models\MasterMerk;
use Illuminate\Http\Request;

class MerkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = MasterMerk::all();
        return view('admin.mobil-merk.merk', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        return view('admin.mobil-merk.form', compact('edit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new MasterMerk();
        $data->nama = $request->name;

        if ($request->file('logo')) {
            $file = $request->file('logo')->store('logo', 'public');
            $data->logo = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('merk.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $data = MasterMerk::find($id);
        return view('admin.mobil-merk.form', compact('edit','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = MasterMerk::find($id);
        $data->nama = $request->name;

        if ($request->file('logo')) {
            $file = $request->file('logo')->store('logo', 'public');
            $data->logo = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('merk.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            MasterMerk::destroy($id);
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('merk.index');
    }
}
