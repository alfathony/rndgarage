<?php

namespace App\Http\Controllers;

use App\Models\Bengkel;
use App\Models\Jasa;
use App\Models\MasterMerk;
use App\Models\MasterSeri;
use App\Models\MasterTipe;
use App\Models\Mekanik;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MekanikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id === 3){
            // cek user ini mempunyai bengkel apa saja
            $bengkelBelongCurrentUser = Bengkel::where('user_id', Auth::user()->id)->pluck('id')->toArray();

            $datas = Mekanik::whereIn('bengkel_id', $bengkelBelongCurrentUser)->get();
        }else {
            $datas = Mekanik::all();
        }
        return view('admin.mekanik.mekanik', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;

        if (Auth::user()->role_id === 3){
            $bengkel = Bengkel::where('user_id', Auth::user()->id)->get();
        }else {
            $bengkel = Bengkel::all();
        }

        $merk = MasterMerk::all();
        return view('admin.mekanik.form', compact('edit', 'bengkel', 'merk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Mekanik();
        $data->nama = $request->name;
        $data->pengalaman = $request->pengalaman;
        $data->bengkel_id = $request->bengkel_id;
        $data->sejak_tahun = $request->sejak_tahun;
        $data->telepon = $request->telepon;

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('mekanik.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $data = Mekanik::find($id);

        if (Auth::user()->role_id === 3){
            $bengkel = Bengkel::where('user_id', Auth::user()->id)->get();
        }else {
            $bengkel = Bengkel::all();
        }

        $merk = MasterMerk::all();
        $tipe = MasterTipe::all();
        $seri = MasterSeri::all();
        return view('admin.mekanik.form', compact('edit','data', 'bengkel', 'merk', 'tipe', 'seri'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Mekanik::find($id);

        $data->nama = $request->name;
        $data->pengalaman = $request->pengalaman;
        $data->bengkel_id = $request->bengkel_id;
        $data->sejak_tahun = $request->sejak_tahun;
        $data->telepon = $request->telepon;

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('mekanik.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Mekanik::destroy($id);
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('mekanik.index');
    }
}
