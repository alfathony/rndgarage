<?php

namespace App\Http\Controllers;

use App\Models\Bengkel;
use App\Models\Jasa;
use App\Models\Mekanik;
use App\Models\SukuCadang;
use App\Models\Transaksi;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalCustomer = User::where('role_id', 1)->count();
        $totalMekanik = Mekanik::all()->count();
        $totalBengkel = User::where('role_id', 3)->count();
        $totalTransaksi = Transaksi::count();
        $totalEarn = Transaksi::where('is_paid', 1)->sum('total');

        $totalJasa = Jasa::count();
        $totalSukuCadang = SukuCadang::count();

        $belumLunas = Transaksi::where('is_paid', 0)->count();
        $lunas = Transaksi::where('is_paid', 1)->count();


        return view('admin.dashboard.dashboard', compact('totalCustomer', 'totalTransaksi', 'totalBengkel', 'totalMekanik', 'belumLunas', 'lunas', 'totalEarn', 'totalJasa', 'totalSukuCadang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
