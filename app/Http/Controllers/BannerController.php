<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\MasterBanner;
use App\Models\MasterFaq;
use Illuminate\Http\Request;

class BannerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = MasterBanner::all();
        return view('admin.banner.list', compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banner.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insert = $request->all();

        // if upload image for web
        if ($request->file('image')) {
            $fileweb = $request->file('image')->store('banner-web','public');
            $insert['image'] = $fileweb;
        }

        MasterBanner::create($insert);

        return redirect()->route('banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banner = MasterBanner::find($id);
        return view('admin.banner.show', compact('banner','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bnr = MasterBanner::find($id);

        return view('admin.banner.edit', compact('bnr','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = MasterBanner::find($id);

        $update->title = $request->title;
        if ($request->file('image')) {
            // check and delete file existing
            if ($request->image && file_exists(storage_path('app/public/banner-web' . $request->image))) {
                Storage::delete('app/public/uni-logo/' . $update->image);
            }
            $file = $request->file('image')->store('banner-web','public');
            $update->image = $file;

        }
        $update->save();

        return redirect()->route('banner.show',$id);

        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $deletebanner = MasterBanner::find($id);
            $deletebanner->delete();
        }catch (\Exception $e){
            report($e);
            return response()->json(false);

        }
        return response()->json(true);

    }

}
