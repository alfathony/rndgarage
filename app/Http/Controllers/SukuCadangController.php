<?php

namespace App\Http\Controllers;

use App\Models\Bengkel;
use App\Models\Jasa;
use App\Models\MasterMerk;
use App\Models\MasterSeri;
use App\Models\MasterTipe;
use App\Models\SukuCadang;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SukuCadangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id === 3){
            // cek user ini mempunyai bengkel apa saja
            $bengkelBelongCurrentUser = Bengkel::where('user_id', Auth::user()->id)->pluck('id')->toArray();

            $datas = SukuCadang::whereIn('bengkel_id', $bengkelBelongCurrentUser)->get();
        }else {
            $datas = SukuCadang::all();
        }
        return view('admin.suku-cadang.suku-cadang', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;

        if (Auth::user()->role_id === 3){
            $bengkel = Bengkel::where('user_id', Auth::user()->id)->get();
        }else {
            $bengkel = Bengkel::all();
        }

        $merk = MasterMerk::all();
        return view('admin.suku-cadang.form', compact('edit', 'bengkel', 'merk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new SukuCadang();
        $data->nama = $request->name;
        $data->harga = $request->harga;
        $data->deskripsi = $request->deskripsi;
        $data->bengkel_id = $request->bengkel_id;
        $data->merk_id = $request->merk_id;
        $data->tipe_id = $request->tipe_id;
        $data->seri_id = $request->seri_id;

        if ($request->file('image')) {
            $file = $request->file('image')->store('images', 'public');
            $data->image = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('suku-cadang.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $data = SukuCadang::find($id);

        if (Auth::user()->role_id === 3){
            $bengkel = Bengkel::where('user_id', Auth::user()->id)->get();
        }else {
            $bengkel = Bengkel::all();
        }

        $merk = MasterMerk::all();
        $tipe = MasterTipe::all();
        $seri = MasterSeri::all();
        return view('admin.suku-cadang.form', compact('edit','data', 'bengkel', 'merk', 'tipe', 'seri'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = SukuCadang::find($id);

        $data->nama = $request->name;
        $data->harga = $request->harga;
        $data->deskripsi = $request->deskripsi;
        $data->bengkel_id = $request->bengkel_id;
        $data->merk_id = $request->merk_id;
        $data->tipe_id = $request->tipe_id;
        $data->seri_id = $request->seri_id;

        if ($request->file('image')) {
            $file = $request->file('image')->store('image', 'public');
            $data->image = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('suku-cadang.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            SukuCadang::destroy($id);
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('suku-cadang.index');
    }
}
