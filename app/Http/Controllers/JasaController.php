<?php

namespace App\Http\Controllers;

use App\Models\Bengkel;
use App\Models\Jasa;
use App\Models\JasaHarga;
use App\Models\MasterKeluhan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JasaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id === 3){
            // cek user ini mempunyai bengkel apa saja
            $bengkelBelongCurrentUser = Bengkel::where('user_id', Auth::user()->id)->pluck('id')->toArray();

            $datas = Jasa::whereIn('bengkel_id', $bengkelBelongCurrentUser)->get();
        }else{
            $datas = Jasa::all();
        }

        return view('admin.jasa.jasa', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;

        $keluhan = MasterKeluhan::orderBy('keluhan', 'ASC')->get();
        if (Auth::user()->role_id === 3){
            $bengkel = Bengkel::where('user_id', Auth::user()->id)->get();
        }else {
            $bengkel = Bengkel::all();
        }

        return view('admin.jasa.form', compact('edit', 'bengkel', 'keluhan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Jasa();
        $data->nama = $request->name;
        $data->harga = $request->harga;
        $data->deskripsi = $request->deskripsi;
        $data->bengkel_id = $request->bengkel_id;
        $data->keluhan_id = $request->keluhan_id;

        if ($request->file('image')) {
            $file = $request->file('image')->store('images', 'public');
            $data->image = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('jasa.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $data = Jasa::find($id);

        $keluhan = MasterKeluhan::orderBy('keluhan', 'ASC')->get();
        if (Auth::user()->role_id === 3){
            $bengkel = Bengkel::where('user_id', Auth::user()->id)->get();
        }else {
            $bengkel = Bengkel::all();
        }

        $jasaHarga = JasaHarga::where('jasa_id', $id)->get();

        return view('admin.jasa.form', compact('edit','data', 'bengkel', 'keluhan', 'jasaHarga'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Jasa::find($id);

        $data->nama = $request->name;
        $data->harga = $request->harga;
        $data->deskripsi = $request->deskripsi;
        $data->bengkel_id = $request->bengkel_id;
        $data->keluhan_id = $request->keluhan_id;

        if ($request->file('image')) {
            $file = $request->file('image')->store('image', 'public');
            $data->image = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('jasa.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Jasa::destroy($id);
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('jasa.index');
    }
}
