<?php

namespace App\Http\Controllers;

use App\Models\MasterKeluhan;
use Illuminate\Http\Request;

class KeluhanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = MasterKeluhan::all();
        return view('admin.keluhan.keluhan', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        return view('admin.keluhan.form', compact('edit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new MasterKeluhan();
        $data->keluhan = $request->keluhan;
        $data->deskripsi = $request->deskripsi;

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('keluhan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $data = MasterKeluhan::find($id);
        return view('admin.keluhan.form', compact('edit','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = MasterKeluhan::find($id);
        $data->keluhan = $request->keluhan;
        $data->deskripsi = $request->deskripsi;

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('keluhan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            MasterKeluhan::destroy($id);
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('keluhan.index');
    }
}
