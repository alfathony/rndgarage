<?php

namespace App\Http\Controllers;

use App\Models\Bengkel;
use App\Models\MasterKota;
use App\User;
use Illuminate\Auth\Access\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BengkelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id === 3){
            $datas = Bengkel::where('user_id', Auth::user()->id)->get();
        }else{
            $datas = Bengkel::all();
        }
        return view('admin.bengkel.bengkel', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        $users = User::where('role_id', 3)->get(); // role admin bengkel
        $kota = MasterKota::orderBy('nama' , 'DESC')->get();
        return view('admin.bengkel.form', compact('edit', 'users', 'kota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Bengkel();
        $data->nama = $request->name;
        $data->alamat = $request->alamat;
        $data->master_kota_id = $request->kota_id;
        $data->deskripsi = $request->deskripsi;
        $data->user_id = $request->user_id;
        $data->telepon = $request->telepon;
        $data->email = $request->email;

        if ($request->file('logo')) {
            $file = $request->file('logo')->store('logo', 'public');
            $data->logo = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('bengkel.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $data = Bengkel::find($id);
        $users = User::where('role_id', 3)->get(); // role admin bengkel
        $kota = MasterKota::orderBy('nama' , 'DESC')->get();
        return view('admin.bengkel.form', compact('edit','data', 'users', 'kota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Bengkel::find($id);
        $data->nama = $request->name;
        $data->master_kota_id = $request->kota_id;

        if ($request->file('logo')) {
            $file = $request->file('logo')->store('logo', 'public');
            $data->logo = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('bengkel.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Bengkel::destroy($id);
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('bengkel.index');
    }
}
