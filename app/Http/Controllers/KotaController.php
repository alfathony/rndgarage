<?php

namespace App\Http\Controllers;

use App\Models\MasterKota;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class KotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = MasterKota::all();
        return view('admin.kota.kota', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        return view('admin.kota.form', compact('edit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new MasterKota();
        $data->nama = $request->name;

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('kota.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $data = MasterKota::find($id);
        return view('admin.kota.form', compact('edit','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = MasterKota::find($id);
        $data->nama = $request->name;

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('kota.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            MasterKota::destroy($id);
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('kota.index');
    }
}
