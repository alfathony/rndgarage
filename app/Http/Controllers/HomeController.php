<?php

namespace App\Http\Controllers;

use App\Models\Bengkel;
use App\Models\Jasa;
use App\Models\JasaHarga;
use App\Models\MasterBanner;
use App\Models\MasterKeluhan;
use App\Models\MasterKota;
use App\Models\MasterMerk;
use App\Models\SukuCadang;
use App\Models\Transaksi;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'mainLayoutType' => 'horizontal'
        ];

        $banners = MasterBanner::orderBy('created_at')->limit('4')->get();
        $jasa = Jasa::orderBy('created_at', 'DESC')->limit(4)->get();
        $sukucadang = SukuCadang::orderBy('created_at', 'DESC')->limit(4)->get();
        $kota = MasterKota::orderBy('created_at', 'DESC')->limit(4)->get();
        $merk = MasterMerk::orderBy('created_at', 'DESC')->limit(6)->get();
        $keluhan = MasterKeluhan::orderBy('created_at', 'DESC')->limit(6)->get();

        return view('home', compact('pageConfigs', 'jasa', 'sukucadang', 'kota', 'merk', 'banners', 'keluhan'));
    }

    public function listJasa()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'mainLayoutType' => 'horizontal'
        ];

        $key = isset($_GET['search']) ? $_GET['search'] : '';
        $keyKota = isset($_GET['kota']) ? $_GET['kota'] : '';

        $jasa = Jasa::where('nama', 'like', '%' . $key . '%')
            ->whereHas('bengkel', function($q) use ($keyKota){
                if ($keyKota){
                    $q->where('master_kota_id', $keyKota);
                }
            })
            ->orderBy('created_at', 'DESC')->get();

        $kota = MasterKota::orderBy('nama', 'ASC')->get();

        return view('jasa-list', compact('pageConfigs', 'jasa', 'kota'));
    }

    public function detailJasa($jasa_id)
    {
        $pageConfigs = [
            'pageHeader' => false,
            'mainLayoutType' => 'horizontal'
        ];

        $jasa = Jasa::find($jasa_id);

        $jasaLainnya = Jasa::orderBy('created_at', 'DESC')->limit(4)->get();

        return view('jasa-detail', compact('pageConfigs', 'jasa', 'jasaLainnya'));
    }

    public function reservasiJasa($jasa_id)
    {
        $pageConfigs = [
            'pageHeader' => false,
            'mainLayoutType' => 'horizontal'
        ];

        $jasa = Jasa::find($jasa_id);
        $user = User::find(Auth::user()->id);

        return view('jasa-reservasi', compact('pageConfigs', 'jasa', 'user'));
    }

    public function storeReservasiJasa($jasa_id, Request $request)
    {
        $jasa = Jasa::find($jasa_id);

        $data = new Transaksi();
        $data->suku_cadang_id = null;
        $data->bengkel_id = $request->bengkel_id;
        $data->jasa_id = $jasa_id;
        $data->jasa_harga_id = $request->jasa_harga_id;
        $data->user_id = Auth::user()->id;
        $data->nama_pelanggan = $request->name;
        $data->email = $request->email;
        $data->telepon = $request->telepon;
        $data->alamat = $request->alamat;
        $data->tanggal_kedatangan = $request->tanggal_kedatangan;
        $data->is_paid = 0;

        if ($request->jasa_harga_id){
            $paket = JasaHarga::find($request->jasa_harga_id);
            $data->nama_paket = $paket->paket;
            $data->total = $paket->harga;
        }else{
            $data->total = 0;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('transaksi.index');
    }

    /* Suku Cadang */

    public function listSukuCadang()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'mainLayoutType' => 'horizontal'
        ];

        $kota = MasterKota::orderBy('nama', 'ASC')->get();

        $key = isset($_GET['search']) ? $_GET['search'] : '';
        $keyKota = isset($_GET['kota']) ? $_GET['kota'] : '';


        $datas = SukuCadang::where('nama', 'like', '%' . $key . '%')
            ->whereHas('bengkel', function($q) use ($keyKota){
                if ($keyKota){
                    $q->where('master_kota_id', $keyKota);
                }
            })
            ->orderBy('created_at', 'DESC')->get();

        return view('suku-cadang-list', compact('pageConfigs', 'datas', 'kota'));
    }

    public function detailSukuCadang($suku_cadang_id)
    {
        $pageConfigs = [
            'pageHeader' => false,
            'mainLayoutType' => 'horizontal'
        ];

        $data = SukuCadang::find($suku_cadang_id);

        $dataLainnya = SukuCadang::orderBy('created_at', 'DESC')->limit(4)->get();

        return view('suku-cadang-detail', compact('pageConfigs', 'data', 'dataLainnya'));
    }

    public function bookingSukuCadang($suku_cadang_id)
    {
        $pageConfigs = [
            'pageHeader' => false,
            'mainLayoutType' => 'horizontal'
        ];

        $jasa = SukuCadang::find($suku_cadang_id);
        $user = User::find(Auth::user()->id);

        return view('suku-cadang-booking', compact('pageConfigs', 'jasa', 'user'));
    }

    public function storeBookingSukuCadang($suku_cadang_id, Request $request)
    {
        $suku_cadang = SukuCadang::find($suku_cadang_id);

        $data = new Transaksi();
        $data->suku_cadang_id = $suku_cadang_id;
        $data->bengkel_id = $request->bengkel_id;
        $data->jasa_id = null;
        $data->user_id = Auth::user()->id;
        $data->nama_pelanggan = $request->name;
        $data->email = $request->email;
        $data->telepon = $request->telepon;
        $data->alamat = $request->alamat;
        $data->is_paid = 0;
        $data->total = $suku_cadang->harga;

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('transaksi.index');
    }

    /* Keluhan */

    public function listKeluhan()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'mainLayoutType' => 'horizontal'
        ];

        $key = isset($_GET['search']) ? $_GET['search'] : '';

        $datas = MasterKeluhan::where('keluhan', 'like', '%' . $key . '%')
            ->orderBy('created_at', 'DESC')->get();

        return view('keluhan-list', compact('pageConfigs', 'datas'));
    }

    public function detailKeluhan(Request $request)
    {
        $pageConfigs = [
            'pageHeader' => false,
            'mainLayoutType' => 'horizontal'
        ];

        $keluhan = MasterKeluhan::findOrFail($request->keluhan_id);
        $detail = Jasa::where('keluhan_id', $request->keluhan_id)->get();

        return view('keluhan-detail', compact('pageConfigs', 'detail', 'keluhan'));
    }

    /* Bengkel */

    public function listBengkel()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'mainLayoutType' => 'horizontal'
        ];

        $key = isset($_GET['search']) ? $_GET['search'] : '';

        if (isset($_GET['kota'])){
            $bengkel = Bengkel::where('nama', 'like', '%' . $key . '%')
                ->where('master_kota_id', $_GET['kota'])
                ->orderBy('created_at', 'DESC')->get();
        }else{
            $bengkel = Bengkel::where('nama', 'like', '%' . $key . '%')
                ->orderBy('created_at', 'DESC')->get();
        }

        $kota = MasterKota::orderBy('nama', 'ASC')->get();

        return view('bengkel-list', compact('pageConfigs', 'bengkel', 'kota'));
    }

    public function detailBengkel($bengkel_id)
    {
        $pageConfigs = [
            'pageHeader' => false,
            'mainLayoutType' => 'horizontal'
        ];

        $bengkel = Bengkel::find($bengkel_id);

        $jasa = Jasa::where('bengkel_id', $bengkel_id)->get();
        $sukucadang = SukuCadang::where('bengkel_id', $bengkel_id)->get();

        return view('bengkel-detail', compact('pageConfigs', 'bengkel', 'jasa', 'sukucadang'));
    }

}
