<?php

namespace App\Http\Controllers;

use App\Models\Bengkel;
use App\Models\Jasa;
use App\Models\Transaksi;
use App\Models\MasterKota;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role_id === 3){
            // cek user ini mempunyai bengkel apa saja
            $bengkelBelongCurrentUser = Bengkel::where('user_id', Auth::user()->id)->pluck('id')->toArray();

            $datas = Transaksi::whereIn('bengkel_id', $bengkelBelongCurrentUser)->get();
        }elseif (Auth::user()->role_id === 1){
            $datas = Transaksi::where('user_id', Auth::user()->id)->get();
        }else{
            $datas = Transaksi::all();
        }

        return view('admin.transaksi.transaksi', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        $users = User::where('role_id', 3)->get(); // role admin transaksi
        $kota = MasterKota::orderBy('nama' , 'DESC')->get();
        return view('admin.transaksi.form', compact('edit', 'users', 'kota'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new Transaksi();
        $data->nama = $request->name;
        $data->alamat = $request->alamat;
        $data->master_kota_id = $request->kota_id;
        $data->deskripsi = $request->deskripsi;
        $data->user_id = $request->user_id;
        $data->telepon = $request->telepon;
        $data->email = $request->email;

        if ($request->file('logo')) {
            $file = $request->file('logo')->store('logo', 'public');
            $data->logo = $file;
        }

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('transaksi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Transaksi::find($id);
        return view('admin.transaksi.detail', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $data = Transaksi::find($id);
        return view('admin.transaksi.form', compact('edit','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Transaksi::find($id);
        $data->is_paid = $request->is_paid;

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('transaksi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Transaksi::destroy($id);
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('transaksi.index');
    }
}
