<?php

namespace App\Http\Controllers;

use App\Models\MasterMerk;
use App\Models\MasterSeri;
use App\Models\MasterTipe;
use Illuminate\Http\Request;

class SeriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = MasterSeri::all();
        return view('admin.mobil-seri.seri', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $edit = false;
        $merk = MasterMerk::all();
        return view('admin.mobil-seri.form', compact('edit', 'merk'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new MasterSeri();
        $data->nama = $request->name;
        $data->tipe_id = $request->tipe_id;

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('seri.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = true;
        $data = MasterSeri::find($id);
        $tipe = MasterTipe::where('merk_id', $data->tipe->merk->id)->get();
        $merk = MasterMerk::all();
        return view('admin.mobil-seri.form', compact('edit','data', 'merk', 'tipe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = MasterSeri::find($id);
        $data->nama = $request->name;
        $data->tipe_id = $request->tipe_id;

        try{
            $data->save();
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('seri.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            MasterSeri::destroy($id);
        }catch (\Exception $e){
            report($e);
            return redirect()->back()->with('status','Ups, something went wrong. Please try again');
        }

        return redirect()->route('seri.index');
    }

    public function getByTipe($tipe_id)
    {
        $datas = MasterSeri::where('tipe_id', $tipe_id)->get();
        foreach ($datas as $data){
            echo "<option value='" . $data->id . "'>" . $data->nama . "</option>";
        }

    }
}
