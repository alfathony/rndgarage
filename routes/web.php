<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/jasa-restorasi', 'HomeController@listJasa')->name('list-jasa');
Route::get('/jasa/{jasa_id}', 'HomeController@detailJasa')->name('detail-jasa');
Route::get('/reservasi-jasa/{jasa_id}', 'HomeController@reservasiJasa')->name('reservasi-jasa');
Route::post('/reservasi-jasa/{jasa_id}', 'HomeController@storeReservasiJasa')->name('reservasi-jasa');

Route::get('/suku-cadang', 'HomeController@listSukuCadang')->name('list-suku-cadang');
Route::get('/suku-cadang/{suku_cadang_id}', 'HomeController@detailSukuCadang')->name('detail-suku-cadang');
Route::get('/booking/{suku_cadang_id}', 'HomeController@bookingSukuCadang')->name('booking-suku-cadang');
Route::post('/booking/{suku_cadang_id}', 'HomeController@storeBookingSukuCadang')->name('booking-suku-cadang');

Route::get('/daftar-keluhan-mobil', 'HomeController@listKeluhan')->name('list-keluhan');
Route::get('/daftar-keluhan-mobil/{keluhan_id}/{slug}', 'HomeController@detailKeluhan')->name('detail-keluhan');

Route::get('/bengkel/{bengkel_id}/{slug}', 'HomeController@detailBengkel')->name('detail-bengkel');
Route::get('/bengkel/', 'HomeController@listBengkel')->name('list-bengkel');

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth']], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::resource('users', 'UsersController');
    Route::resource('kota', 'KotaController');
    Route::resource('roles', 'RolesController');
    Route::resource('merk', 'MerkController');
    Route::resource('tipe', 'TipeController');
    Route::resource('seri', 'SeriController');
    Route::resource('bengkel', 'BengkelController');
    Route::resource('jasa', 'JasaController');

    Route::get('jasa-harga/{jasa_id}', 'JasaHargaController@create')->name('jasa-harga-form');
    Route::post('jasa-harga/{jasa_id}', 'JasaHargaController@store')->name('jasa-harga-store');
    Route::get('jasa-harga/{jasa_id}/{jasa_harga_id}', 'JasaHargaController@edit')->name('jasa-harga-edit');
    Route::put('jasa-harga/{jasa_id}/{jasa_harga_id}', 'JasaHargaController@update')->name('jasa-harga-update');
    Route::get('jasa-harga-hapus/{jasa_harga_id}', 'JasaHargaController@destroy')->name('jasa-harga-delete');

    Route::resource('suku-cadang', 'SukuCadangController');
    Route::resource('transaksi', 'TransaksiController');
    Route::resource('banner', 'BannerController');
    Route::resource('mekanik', 'MekanikController');
    Route::resource('keluhan', 'KeluhanController');
});

// call with ajax
Route::get('/ajax/get-tipe-by-merk/{merk_id}', 'TipeController@getByMerk')->name('ajax.get-tipe-by-merk');
Route::get('/ajax/get-seri-by-tipe/{tipe_id}', 'SeriController@getByTipe')->name('ajax.get-seri-by-tipe');

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('logs');

Route::get('/foo', function () {
    Artisan::call('storage:link');
});

