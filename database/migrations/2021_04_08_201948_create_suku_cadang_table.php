<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSukuCadangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suku_cadang', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
            $table->integer('harga')->nullable();
            $table->text('deskripsi')->nullable();
            $table->foreignId('bengkel_id')->references('id')->on('bengkel');
            $table->foreignId('merk_id')->references('id')->on('master_merk');
            $table->foreignId('tipe_id')->references('id')->on('master_tipe');
            $table->foreignId('seri_id')->references('id')->on('master_seri');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suku_cadang');
    }
}
