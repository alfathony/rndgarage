<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDataToTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksi', function (Blueprint $table) {
            $table->foreignId('jasa_id')->references('id')->on('jasa');
            $table->foreignId('user_id')->references('id')->on('users');
            $table->string('nama_pelanggan')->nullable();
            $table->string('email')->nullable();
            $table->string('telepon')->nullable();
            $table->string('alamat')->nullable();
            $table->dateTime('tanggal_kedatangan')->nullable();
            $table->tinyInteger('is_paid')->nullable();
            $table->integer('total')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksi', function (Blueprint $table) {
            $table->dropColumn('jasa_id');
            $table->dropColumn('user_id');
            $table->dropColumn('nama_pelanggan');
            $table->dropColumn('email');
            $table->dropColumn('telepon');
            $table->dropColumn('alamat');
            $table->dropColumn('tanggal_kedatangan');
            $table->dropColumn('is_paid');
            $table->dropColumn('total');
        });
    }
}
