<?php

use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            [
                'name' => 'Joko User',
                'role_id' => 1,
                'email' => 'user@rndgarage.com',
                'password' => bcrypt('asdfasdf'),
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Agung Admin',
                'role_id' => 2,
                'email' => 'admin@rndgarage.com',
                'password' => bcrypt('asdfasdf'),
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Udin Bengkel',
                'role_id' => 3,
                'email' => 'bengkel@rndgarage.com',
                'password' => bcrypt('asdfasdf'),
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
